;(function(){

	describe('Form Validation', function(){
		
		var formInstance, form, formElements;

		beforeEach(function(){
			//Load in the mock HTML
			loadFixtures('form.html');
			//Create a new FormValidator instance
			formInstance = new propLibs.FormValidator({
				formID: 'example-form'
			});

			formElements = document.forms['example-form'].elements;
			form = document.forms['example-form'];
		});

		//Clear the form values after it's been finished
		afterEach(function(){
			for(var i = 0; i < form.length; i++){
				formElements[i].value = '';
			}
		});

		it('Creates an instance of FormValidator', function(){
			expect(typeof formInstance).toBe('object');
		});

		it('Throws an error if we try to validate an element that doesnt exist', function(){ 
			expect(formInstance.validateByName.bind(formInstance, 'nllnlef')).toThrow();
			expect(formInstance.validateByName.bind(formInstance)).toThrow();
		});

		it('should run the validatiom if the value has been changed', function(){
			var element = formElements['email'];
			element.value = 'test'
			//Dispatch the onchange element
			element.dispatchEvent(new Event('change'));
			expect(element).toHaveClass('error');
		});

		describe('bad', function(){

			describe('text Inputs', function(){

				it('returns error if length is less than 1 and is a required field', function(){
					formElements['text'].required = true;
					expect(formInstance.validateByName('text')).toEqual(jasmine.arrayContaining(['The text field is required']));
				});

			})

			describe('Email inputs', function(){

				it('Returns error if length is less than 1', function(){
					formElements['email'].required = true;
					expect(formInstance.validateByName('email')).toEqual(jasmine.arrayContaining(['The email field is required']));
				});

				it('value conforms to general conventions test@test.com', function(){
					// form['email'].required = true;
					formElements['email'].value = 'test';
					expect(formInstance.validateByName('email')).toEqual(jasmine.arrayContaining(['Please ensure email is in the form email@host.com']));
				});

				it('value conforms to min length', function(){
					formElements['email'].value = 't';
					expect(formInstance.validateByName('email')).toEqual(jasmine.arrayContaining(['The email field is required']));
				});

			});

		});

		describe('good', function(){

			describe('text inputs', function(){

				it('returns true if there is a value on the element', function(){
					formElements['text'].value = 'test';
					expect(formInstance.validateByName('text')).toBe(true);
				})

			})

			describe('Email inputs', function(){

				it('returns true if the value conforms to general conventions for email addresses test@test.com', function(){
					formElements['email'].value = 'test@test.com';
					expect(formInstance.validateByName('email')).toBe(true);
				});

			});


		});

		describe('submit form', function(){

			it('on submit fails when there is no data and required fields', function(){
				formElements[0].required = true;
				expect(form.onsubmit()).toBe(false);
			});

			it('on submit passes when there is data and we aren\'t using AJAX', function(){
				//Force the useAJAX setting to be false
				formInstance._settings.useAJAX = false;
				expect(form.onsubmit()).toBe(true);
			});

		});

		describe('test the enable/disable functions', function(){
			
			it('when disabled to have an opacity of 0.5, and form elements to be disabled', function(){

				formInstance.disable();
				
				expect(form).toHaveCss({'opacity': '0.5'});
				for(var i = 0; i < formElements.length; i++){
					element = formElements[i];
					expect(element).toBeDisabled();
				}
			});

			it('when disabled to have an opacity of 0.5, and form elements to be enabled', function(){

				formInstance.enable();
				
				expect(form).toHaveCss({'opacity': '1'});
				for(var i = 0; i < formElements.length; i++){
					element = formElements[i];
					expect(element).not.toBeDisabled();
				}
			});

			it("should disable a particular element by name", function(){
				var name = 'email';
				
				formInstance.toggleElement(name);


				for(var i = 0; i < formElements.length; i++){
					element = formElements[i];

					if(element.name === name){
						expect(element).toBeDisabled();
					} 
				}
			
			});

			it('should still be toggled disabled if I have enabled/disabled a form', function(){
				var name = 'text'
				formInstance.toggleElement(name);

				formInstance.disable();
				formInstance.enable();

				expect(formElements[name]).toBeDisabled();

			});
		
		});

		describe('show a list of errors attached to the label element', function(){
			
			it('should return a list of labels for a particular input ', function(){
				expect(formInstance._getLabels('text').length).toBe(0);
				expect(formInstance._getLabels('email').length).toBe(1);
			});

			it('should show an error message inside the label element', function(){
				formElements['email'].value = 'test';
				formInstance.validate();

				var labels = document.getElementsByTagName('label');

				for(var i = 0; i < labels.length; i++){
					var label = labels[i];
					if(label.htmlfor === 'email'){
						expect(label).toContain('span');
					} else {
						expect(label).not.toContain('span');
					}
				}
			});

		});

		describe('AJAX Functionality', function(){
			it("should receive a successful response", function() {
				spyOn(XMLHttpRequest.prototype, 'open').and.callThrough();
  				spyOn(XMLHttpRequest.prototype, 'send');

  				formInstance._submitForm();

  				expect(XMLHttpRequest.prototype.open).toHaveBeenCalled();
			});

		});


	});



	//ToDo Add in tests for error functions - extra use cases. 

}());
