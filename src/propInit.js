/**
 * @author: Sam Woodbridge
 */

;(function(window){

	'use strict';

	var propCore = {};

	/**
	 * Called when all dependencies have been loaded.
	 */
	var init = function(){
		initFromSettings();
		bindEvents(window.propFuncs);
	};

	/**
	 * This will init all the classes as defined in the settings object
	 */
	var initFromSettings = function(){ 
		for(var propSetting in propSettings){
			//Make sure we are in the root of the object.
			if(propSettings.hasOwnProperty(propSetting)){
				var settingName = capitalizeFirstLetter(propSetting);
				try{
					propCore[settingName] = new propLibs[settingName](propSettings[propSetting]); 

				} catch(err) {
					//Do some prettif
					if(err.message === 'propLibs[settingName] is not a function'){
						console.error('No class found with the name: '+settingName);
					} else {
						console.error(err);
					}
				}
			}
		}
	}; 


	var capitalizeFirstLetter = function(string) { 
	    return string.charAt(0).toUpperCase() + string.slice(1);
	};

	/**
	 * Removes the __ in a name of the function, so we can get the name and action to bind the function to. 
	 * @param operation
	 * @returns {{name: *, action: *, operation: *}}
	 */
	var formatName = function(name) {
		//Just return the name of the event if we don't have 
		var result = name.split('__');
		return {name: result[0], action: result[1], operation: name}; 
	};

	/**
	 * loop through the events and bind the neccesary one
	 * @param  {Object} events An object of events with  the key of Name__Action
	 */
	var bindEvents = function(events){
		if(events){  
			for(var operation in events){
				var eventInfo = formatName(operation);
				bindEvent(eventInfo, events[operation]); 
			}
		}
		window.propCore = propCore;
	};

	/**
	 * Binds a function onto an event - as defined by the settings object
	 * @param  {Object} eventInfo Information about what event to bind onto
	 * @param  {String} operation The function to call
	 */
	var bindEvent = function(eventInfo, operation){
		try {
			//Set up the function into the global propCore namespace
			propCore[eventInfo.name] = new propFuncs[eventInfo.operation]();
			//Only bind if we have to scroll or resize.
			if(eventInfo.action === 'scroll' || eventInfo.action === 'resize') {
				//Do the bind
				$(window).on(eventInfo.action, function(e) { 
					//Use window.requestAnimationFrame to throttle the amount of times the function is called
					if(window.requestAnimationFrame) {
						window.requestAnimationFrame(propCore[eventInfo.name][eventInfo.action].bind(this, e));
					} else {
						propCore[eventInfo.name](e);
					}
				});
			}
		} catch(error) {
			if(console.groupCollapsed) {
				console.groupCollapsed('%c ['+ operation +' error] - ' + error['message'] + '. Expand for details:', 'color: #ff5a5a');
				console.info('%c '+error['stack'], 'color: #ff5a5a');
				console.groupEnd();
			}
		}
	};

	window.init = init;
	window.proCore = propCore;
	window.propLibs = window.propLibs || {};
	window.propFuncs = window.propFuncs || {};

})(window);