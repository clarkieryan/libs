/**
 * FormValidator Module
 * @description This module provides validation functionality for any form elements on the page
 * @class propLibs.FormValidator
 * @param {object} settings Settings object to be merged with defaultSettings
 * @example
 *  new propLibs.FormValidator({
 *  	formID: 'test-element'
 *  });
 * @author Ryan Clarke <ryan.clarke@propcom.co.uk>
 */

;(function(){

	'use strict';

	var pluginInfo = {
		name: 'FormValidator',
		version: 0.1
	};

	/**
	 * Default settings
	 * @type {Object}
	 */
	var defaultSettings = {
		formID: '#form', 
		useAJAX: true,
		fieldLengths: {
			'email': 3, 
			'password': 6, 
			'name': 1, 
			'phone': 5
		}, 
		customURL: null,
		extraValidation: {},
		serialize: true, 
		textSelectors: {
			welcome: '.welcomeText', 
			error: '.errorText'
		},
		callbacks: {
			success: null, 
			error: null, 
			beforeSend: null
		}, 
		customValidation: {}
	};

	/**
	 * FormValidator instancr
	 * @constructor
	 * @param {Object} overrideSettings Settings file provided to override the default settings
	 */
	var FormValidator = function(overrideSettings){
		//ToDo: Remove this JQuery depedancy
		this._settings = $.extend({}, defaultSettings, overrideSettings);

		if(document.forms[this._settings.formID]){
			this._selectors = {
				form: document.forms[this._settings.formID],
				formElements: document.forms[this._settings.formID].elements, 
				errorText: document.querySelector(this._settings.textSelectors.error), 
				welcomeText: document.querySelector(this._settings.textSelectors.welcome), 
				formContainer: document.getElementById(this._settings.formID)
			};

			this._disabled = false;

			//Force the form not to use the HTML5 validation rules
			this._selectors.form.setAttribute('novalidate', '');

			this._validationFunctions = $.extend(true, {}, defaultValidationFunctions, this._settings.customValidation);

			this._bindEvents();
		}
	};

	/**
	 * Get's the plugin information
	 * @public
	 * @method getPluginInfo
	 * @memberOf propLibs.FormValidation
	 * @return {Object} An object of the stored info about this plugin
	 */
	FormValidator.prototype.getPluginInfo = function(){
		return pluginInfo;
	};


	/**
	 * Binds the neccesary events onto all of the form elements
	 * @memberOf propLibs.FormValidation
	 * @private
	 * @method  _bindEvents
	 */
	FormValidator.prototype._bindEvents = function(){
		this._selectors.form.onsubmit = this._submitForm.bind(this);

		for(var i = 0; i < this._selectors.formElements.length; i++){
			var element = this._selectors.formElements[i];
			element.onchange = this._onChange.bind(this);
			element.onblur = this._onBlur.bind(this);
		}
	};

	/**
	 * Called whenever a field element changes
	 * @private
	 * @method _onChange
	 * @memberOf propLibs.FormValidation
	 * @param  {Object} event Event object passed when the event is triggered
	 */
	FormValidator.prototype._onChange = function(event){
		//Do some validation on the element
		this._validateElement(event.target);
	};

	
	/**
	 * Called whenever an input losses focus
	 * @param  {Object} event Event passed when the event is invoked
	 * @private
	 * @method  _onBlur
	 * @memberOf propLibs.FormValidation
	 */
	FormValidator.prototype._onBlur = function(event){
		this._validateElement(event.target);
	};

	/**
	 * Does the submit action
	 * @private
	 * @method  _submitForm
	 * @memberOf propLibs.FormValidator
	 * @return {Boolean} Returns true/false based on if it's passed validation.
	 */
	FormValidator.prototype._submitForm = function(){
		//If then don't send the form
		if(!this._disabled){
			if(!this.validate()){
				return false;
			} else if(this._settings.useAJAX){
				this._submitAJAXForm();
				return false;
			}

			return true;
		} 
		return false;
	};

	FormValidator.prototype._submitAJAXForm = function(){
		var data = this._settings.serialize ? $(this._selectors.form).serialize() + '&submitted=Send' : null;
		var url = this._settings.customURL || this._selectors.form.getAttribute('action');
		
		var request = new XMLHttpRequest();
		request.open('POST', url, true);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		request.send(data);

		//Need to do the before post function here

		request.addEventListener("load", this._onAJAXSuccess.bind(this));
		request.addEventListener("error", this._onAJAXError.bind(this));
	};

	/**
	 * Called just before the data is sent to the server on an AJAX request
	 * @private
	 * @method _onAJAXBeforeSend
	 * @memberOf propLibs.FormValidator
	 */
	FormValidator.prototype._onAJAXBeforeSend = function(){
		if(this._selectors.welcomeText){
			this._selectors.welcomeText.innerHTML = 'Sending...';
		}

		this.disable();

		if(this._settings.call.beforeSend){
			this._settings.callbacks.beforeSend.call(this);
		}

		return true;
	};

	/**
	 * Called when we get a success from an AJAX request
	 * @private
	 * @method _onAJAXSuccess
	 * @memberOf propLibs.FormValidator
	 * @param  {Object} data data received from the AJAX request
	 */
	FormValidator.prototype._onAJAXSuccess = function(data) {

		//Convert the response into a HTML node.
		var parser = new DOMParser();
		var response = parser.parseFromString(data.target.response, "text/html");

		var error = response.querySelector('.errorText') ? response.querySelector('.errorText').innerHTML : null;
		var success = response.querySelector('.successText') ? response.querySelector('.successText').innerHTML : null;

		if(error){
			if(this._selectors.errorText){
				this._selectors.errorText.innerHTML = error;
			}
		} else if(success){
			if(this._selectors.formContainer){
				this._selectors.formContainer.innerHTML = success;	
				this._selectors.welcomeText.innerHTML = '';
			}
		} else {
			alert('Something went wrong');
		}

		this.enable();
		
		if(this._settings.callbacks.success){
			this._settings.callbacks.success.call(this);			
		}
	};

	/**
	 * Called when we get an error from the AJAX request
	 * @private
	 * @method _onAJAXError
	 * @memberOf propLibs.FormValidator
	 * @param  {Object} error error handler passed from ajax method
	 */
	FormValidator.prototype._onAJAXError = function(error){
		this.enable();

		if(this._selectors.callbacks.error){
			this._settings.callbacks.error.call(this, error);
		}
	};

	/**
	 * Validates the form if any errors are present it will show these.
	 * @public
	 * @method  validate
	 * @memberOf propLibs.FormValidator
	 * @return {Boolean} Wether the form is valid or not
	 */
	FormValidator.prototype.validate = function(){
		var formValid = true;

		for(var i = 0; i < this._selectors.formElements.length; i++){
			var option = this._selectors.formElements[i];
			var validationOutput = this._validateElement(option);
			if(validationOutput !== true){
				formValid  = false;
			} 
		}

		return formValid;
	};

	/**
	 * Validate a form element by the form name
	 * @public
	 * @method  validateByName
	 * @memberOf propLibs.FormValidator
	 * @param  {String} name The name of the form element
	 * @return {Boolean|Array}      Either returns true, or an Array of errors.
	 */
	FormValidator.prototype.validateByName = function(name){
		//Make sure there is an element first
		if(this._selectors.formElements[name] !== undefined){
			return this._validateElement(this._selectors.formElements[name]);
		} else {
			throw {
				name: "Error",
				message: "Form element does not exist"
			};
		}
	};

	/**
	 * Validate the element that's been passed to it
	 * @private
	 * @method  _validateElement
	 * @memberOf propLibs.FormValidator
	 * @param  {Object} element HTML Element instance of the form element to be validated
	 * @return {Boolean|Array}          Either returns true, or an array of errors. 
	 */
	FormValidator.prototype._validateElement = function(element){
		element.errorMessages = [];
		this._removeErrorMessages(element);

		//Only run the validation, if there is a value or it's a required field.
		if(element.required || element.value.length > 0){
			this._lengthValidation(element);
			
			if(this._validationFunctions[element.type]){
				if(element.errorMessages.length > 0 || !this._validationFunctions[element.type](element)){
					element.classList.add('error');
					//If we have an error but no error message has been added let's add a generic one
					if(element.errorMessages.length < 1){
						element.errorMessages.push('Please check the '+element.nane+' field again.');
					}
					this._addErrorMessages(element);
				} else {
					element.classList.remove('error');
					this._removeErrorMessages(element);
				}
			}
		}

		//Either return the error messages if we have any else return true;
		return element.errorMessages.length > 0 ? element.errorMessages : true;	
	};

	/**
	 * Ensures that there has been a value inputted if the required tag is there, else let's it through;
	 * @private
	 * @method  _lengthValidation
	 * @memberOf propLibs.FormValidator
	 * @param  {Object} element      HTML Element instance of the form element to be checked.
	 * @param  {Array} errorMessages Array of error messages to append any more to it.
	 */
	FormValidator.prototype._lengthValidation = function(element){
		var fieldLengths = this._settings.fieldLengths[element.type] || 1;

		if(element.value.length < fieldLengths){
			element.errorMessages.push('The '+element.name+' field is required');
		}

	};

	/**
	 * Adds the user viewable error messages
	 * @method _addErrorMessages
	 * @private
	 * @memberOf propLibs.FormValidator
	 * @param {Object} element HTML Node of the element to append the errors to. 
	 */
	FormValidator.prototype._addErrorMessages = function(element){
		var labels = this._getLabels(element);
		//For every label add the error message string
		for(var i = 0; i < labels.length; i++){
			var label = labels[0];

			var errorMessage = document.createElement("span");
			errorMessage.classList.add('error');
			errorMessage.textContent = element.errorMessages.join(' , ');
			label.appendChild(errorMessage);
		}
	};

	/**
	 * Removes the errormessages from the current form. 
	 * @method  _removeErrorMessages
	 * @private
	 * @memberOf propLibs.FormValidator
	 * @param  {Object} element The HTML Node element
	 */
	FormValidator.prototype._removeErrorMessages = function(element){
		var labels = this._getLabels(element);
		for(var i = 0; i < labels.length; i++){
			var label = labels[i];
			var errors = label.getElementsByTagName('span');
			
			for(var x = 0; x < errors.length; x++){
				label.removeChild(errors[x]);
			}	
		}
	};


	/**
	 * Changes opacity of the form ot 0.5 and disables the form elements
	 * @public
	 * @memberOf propLibs.FormValidator
	 * @method  disable
	 */
	FormValidator.prototype.disable = function(){
		this._disabled = true;
		this._selectors.form.style.opacity = 0.5;
		for(var i = 0; i < this._selectors.formElements.length; i++){
			var element  = this._selectors.formElements[i];
			this._setElementState(element, true);
		}
	};

	/**
	 * Set's the opacity back to 1 and enables the form elements
	 * @public
	 * @memberOf propLibs.FormValidator
	 * @method  enable
	 */
	FormValidator.prototype.enable = function(){
		this._disabled = false;
		this._selectors.form.style.opacity = 1;
		for(var i = 0; i < this._selectors.formElements.length; i++){
			var element  = this._selectors.formElements[i];
			this._setElementState(element, false);
		}
	};

	/**
	 * Toggles a particular element to be disabled or not. 
	 * @param  {String|Object} element Name or HTML Node of the element
	 * @param {Boolean} forceState If left undefined will toggle, otherwise will force it to be state give, true/false
	 * @public
	 * @method  toggleElement
	 * @memberOf propLibs.FormValidator
	 */
	FormValidator.prototype.toggleElement = function(elementName){
		var element = this._selectors.formElements[elementName];
		element.staticState = !element.staticState;
		this._setElementState(element, !element.disabled, true);
	};

	/**
	 * Sets an element state, unless it's set by the toggle
	 * @method  _setElementState
	 * @private
	 * @memberOf propLibs.FormValidator
	 * @param {Object} element HTML Node of what to toggle the state of
	 */
	FormValidator.prototype._setElementState = function(element, state, force){
		//Only allow if the static state does not equal true
		if(force || element.staticState !== true){
			element.disabled = state;
		}
	};

	/**
	 * Resets the whole form back to the original onload state
	 * @public
	 * @method  reset
	 * @memberOf propLibs.FormValidator
	 */
	FormValidator.prototype.reset = function(){

		for(var i = 0; i < this._selectors.formElements.length; i++){
			var element = this._selectors.formElements[i];
			element.value = '';
			this._removeErrorMessages(element);
		}

	};

	/**
	 * Cross-Browser friendly way of getting all of the labels
	 * @method  _getLabels
	 * @private
	 * @memberOf propLibs.FormValidator
	 * @param  {Obejct} element HTML Node of the input you want to get the labels of
	 * @return {Array}         An array of HTML Nodes that are for the inputted HTML Node
	 */
	FormValidator.prototype._getLabels = function(elementName){
		var element;
		var labels = [];

		if(typeof elementName === 'string'){
			element = this._selectors.form[elementName];
		} else {
			element = elementName;
		}

		var documentLabels = document.getElementsByTagName('label');
		for(var i = 0; i < documentLabels.length; i++){
			var label = documentLabels[i];
			if(label.htmlFor === element.name || label.htmlFor === element.id){
				labels.push(label);
			}
		}

		return labels;
	};

	/**
	 * Object of validation for certain field types
	 * @type {Object}
	 */
	var defaultValidationFunctions = {
		'email': function(element){
			var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(!re.test(element.value)){
				element.errorMessages.push('Please ensure email is in the form email@host.com');
			}
    		return re.test(element.value);
		}, 
		'text': function(element){
			return element.value.length > -1;
		}, 
		'tel': function(element){
			return element.value.length > -1;
		}, 
		'textarea': function(element){
			return element.value.length > -1;
		}
	};


	propLibs.FormValidator = FormValidator;

})();