/**
 * GoogleMaps Module
 * @description This module creates a series of GoogleMap instances and stores a refrence to them. You can either provide an array of settings or a single setting object. 
 * @class propLibs.GoogleMaps
 * @param {object|array} settings Settings object or array of objects to be passed to the GoogleMap instance on instantiation.
 * @see propLibs.GoogleMap
 * @example
 * 	var googleMaps = new propLibs.GoogleMaps([{}, {}, {}]);
 * 	var googleMaps = new propLibs.GoogleMaps({});
 * @author Ryan Clarke <ryan.clarke@propcom.co.uk>
 */

;(function(){

	'use strict';

	/**
	 * A collection of GoogleMap instances
	 * @param {object|array} settings Settings object or an array of objects
	 */
	var GoogleMaps = function(settings){
		this._maps = [];
		this._settings = settings;
		

		if(this._settings && this._settings instanceof Array){
			this._addMaps(this._settings);
		} else if(this._settings) {
			this._addMap(this._settings);
		}

		this._addScript();
	};

	/**
	 * Loops through the settings and adds in the map object;
	 * @method _addMaps
	 * @private
	 * @memberOf propLibs.GoogleMaps
	 */
	GoogleMaps.prototype._addMaps = function() {
		for(var i = 0; i < this._settings.length; i++){
			this._addMap(this._settings[i]);	
		}
	};

	/**
	 * Creates the instance and pushes into the array
	 * @method _addMap
	 * @memberOf propLibs.GoogleMaps
	 * @private
	 * @param {object} mapSettings object to be pushed to the GoogleMap class.
	 */
	GoogleMaps.prototype._addMap = function(mapSettings){
		var map = new propLibs.GoogleMap(mapSettings);
		this._maps.push(map);
	};

	/**
	 * Calls the init function of all the map objects created. 
	 * @method init
	 * @memberOf propLibs.GoogleMaps
	 */
	GoogleMaps.prototype.init = function(){
		for(var i = 0; i < this._maps.length; i++){
			this._maps[i].init();
		}
	};

	/**
	 * This will pull in the google map libraries 
	 * @method _addScript
	 * @private
	 * @memberOf propLibs.GoogleMaps
	 */
	GoogleMaps.prototype._addScript = function() {
		var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&callback=propCore.GoogleMaps.init';

		document.body.appendChild(script);
	};


	propLibs.GoogleMaps = GoogleMaps;

})();