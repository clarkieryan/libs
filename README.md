# Propeller JS Libraries

### [Docs](http://proplibs.dev) [JSDoc](http://proplibs.dev/jsdoc) [Tests](http://proplibs.dev/test) 

propLibs hold's all of our custom libraries, currently we have the following;

(How to use guide underneath)

- propInit
	- Automatically instantiates any class defined in propSetting, and on ready any function loaded via propFuncs 
- FormValidator
	- Adds validation to either specified forms or all forms on the current page.  
- GoogleMaps
	- GoogleMaps is a collection of GoogleMap instances
- GoogleMap
	- GoogleMap shows a single google map into a predefined div it accepts a settings object
	
		